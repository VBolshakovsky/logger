from setuptools import find_packages, setup

setup(
    name='logger',
    packages=find_packages(),
    version='0.1.0',
    description='Logger',
    author='V.Bolshakov',
)