# logger

Проект для работы с Logger-ом python

## Установка

1. Создайте папку для проекта:
```
mkdir logger
cd logger
```
2. Склонируйте репозиторий: 
```
git clone https://gitlab.com/VBolshakovsky/logger.git
```
3. Создаем виртуальное окружение
Windows:
```
python -m venv venv
```
Linux:
```
python3 -m venv venv
```
3. Активируем виртуальное окружение
Windows:
```
venv\Scripts\Activate.ps1
```
Linux:
```
source env/bin/activate
```
3. Обновим pip
```
python -m pip install --upgrade pip
```
4. Установите необходимые пакеты: 
```
pip install -r .\logger\requirements\base.txt
```
5. Запустите OpenSearch в докере
```
docker compose -f "logger\docker-compose.yml" up -d --build
```

## Использование

logger\src\service\logger\example.py

## Лицензия

No license file
