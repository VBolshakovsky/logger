from os import environ
from dotenv import load_dotenv

load_dotenv()


class Settings:
    #Общие
    APP_NAME = environ.get('APP_NAME', 'logger')
    TIME_ZONE = environ.get('TIME_ZONE', 'Europe/Moscow')

    #Логирование
    LOG_DATEFMT = environ.get('LOG_DATEFMT', '%d.%m.%Y %H:%M:%S [%Z]')
    LOG_FMT = environ.get(
        'LOG_FMT', '%(asctime)s: [%(name)s|%(levelname)s]: %(message)s')
    LOG_PATH = environ.get('LOG_PATH', 'logger/log')

    #OpenSearch
    OPEN_SEARCH_SERVER = environ.get('OPEN_SEARCH_HOST', 'localhost')
    OPEN_SEARCH_PORT = environ.get('OPEN_SEARCH_PORT', '9200')
    OPEN_SEARCH_HOST = f"{OPEN_SEARCH_SERVER}:{OPEN_SEARCH_PORT}"  #"https://localhost:9200"
    OPEN_SEARCH_INDEX = environ.get('OPEN_SEARCH_INDEX', APP_NAME.lower())
    OPEN_SEARCH_USER = environ.get('OPEN_SEARCH_USER', 'admin')
    OPEN_SEARCH_PASSWORD = environ.get('OPEN_SEARCH_PASSWORD', 'admin')
