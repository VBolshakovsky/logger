import logging
from logger import init_logger
from src.config import Settings as settings

#Подключаем логер
init_logger(console=True, file_full=True, open_search=False)
logger = logging.getLogger(settings.APP_NAME)

logger.debug('Это отладочное сообщение.')
logger.info('Это информационное сообщение.')
logger.warning('Это предупреждающее сообщение.')
logger.error('Это сообщение об ошибке.')
logger.critical('Это сообщение о критической ошибке.')