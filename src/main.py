import logging
from src.service.logger.logger import init_logger
from src.config import Settings as settings

print(settings.APP_NAME)

init_logger()
logger = logging.getLogger(settings.APP_NAME)

logger.debug('Это отладочное сообщение.')
logger.info('Это информационное сообщение.')
logger.warning('Это предупреждающее сообщение.')
logger.error('Это сообщение об ошибке.')
logger.critical('Это сообщение о критической ошибке.')
